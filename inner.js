//Parte de teste de vitória;

//TODO método de vitória vertical;let cont_azul = 0;
let cont_azul = 0;
let cont_vermelho = 0;
let tabela_vermelho = document.createElement("div");
tabela_vermelho.className = "vermelho_placar";
document.getElementById("placar_container").appendChild(tabela_vermelho);
let tabela_azul = document.createElement("div");
tabela_azul.className = "azul_placar";
document.getElementById("placar_container").appendChild(tabela_azul);

function vitoriaV(stringmap, playeringame) {
  for (let j = 0; j <= 6; j++) {
    for (let i = 5; i >= 3; i--) {
      if (
        stringmap[i][j] == stringmap[i - 1][j] &&
        stringmap[i - 2][j] == stringmap[i - 3][j] &&
        stringmap[i - 1][j] == stringmap[i - 3][j]
      ) {
        const winarea = document.getElementById("Winner_annoucement");
        while (winarea.firstChild) {
          winarea.removeChild(winarea.firstChild);
        }
        if (playeringame == "P") {
          const winmessage = document.createTextNode("AZUL VENCEU!!!");
          winarea.appendChild(winmessage);
          const wins = new Audio("./sounds/winsong.wav");
          wins.play();
          cont_azul++;
          setTimeout(mapReset, 1500);
        } else if (playeringame == "V") {
          const winmessage = document.createTextNode("VERMELHO VENCEU!!!");
          winarea.appendChild(winmessage);
          const wins = new Audio("./sounds/winsong.wav");
          wins.play();
          cont_vermelho++;
          setTimeout(mapReset, 1500);
        }
      }
    }
  }
}
//TODO método de vitória horizontal;
function vitoriaH(stringmap, playeringame) {
  for (let i = 5; i >= 0; i--) {
    for (let j = 0; j <= 3; j++) {
      // console.log(stringmap[i][j]);
      if (
        stringmap[i][j] == stringmap[i][j + 1] &&
        stringmap[i][j + 2] == stringmap[i][j + 3] &&
        stringmap[i][j + 1] == stringmap[i][j + 3]
      ) {
        const winarea = document.getElementById("Winner_annoucement");
        while (winarea.firstChild) {
          winarea.removeChild(winarea.firstChild);
        }
        if (playeringame == "V") {
          const winmessage = document.createTextNode("AZUL VENCEU!!!");
          winarea.appendChild(winmessage);
          const wins = new Audio("./sounds/winsong.wav");
          wins.play();
          cont_azul++;
          setTimeout(mapReset, 1500);
        } else if (playeringame == "P") {
          const winmessage = document.createTextNode("VERMELHO VENCEU!!!");
          winarea.appendChild(winmessage);
          const wins = new Audio("./sounds/winsong.wav");
          wins.play();
          cont_vermelho++;
          setTimeout(mapReset, 1500);
        }
      }
    }
  }
}
//TODO método de vitória diagonal;
function vitoriaD(stringmap, playeringame) {
  for (let i = 5; i >= 3; i--) {
    for (let j = 0; j <= 3; j++) {
      if (
        stringmap[i][j] == stringmap[i - 1][j + 1] &&
        stringmap[i][j] == stringmap[i - 3][j + 3] &&
        stringmap[i][j] == stringmap[i - 2][j + 2]
      ) {
        const winarea = document.getElementById("Winner_annoucement");
        while (winarea.firstChild) {
          winarea.removeChild(winarea.firstChild);
        }
        if (playeringame == "P") {
          const winmessage = document.createTextNode("AZUL VENCEU!!!");
          winarea.appendChild(winmessage);
          cont_azul++;
          const wins = new Audio("./sounds/winsong.wav");
          wins.play();
          setTimeout(mapReset, 1500);
          
        } else if (playeringame == "V") {
          const winmessage = document.createTextNode("VERMELHO VENCEU!!!");
          winarea.appendChild(winmessage);
          cont_vermelho++;
          const wins = new Audio("./sounds/winsong.wav");
          wins.play();
          setTimeout(mapReset, 1500);
          
        }

      } else if (
        stringmap[i - 3][j] == stringmap[i - 2][j + 1] &&
        stringmap[i - 3][j] == stringmap[i][j + 3] &&
        stringmap[i - 3][j] == stringmap[i - 1][j + 2]
      ) {
        const winarea = document.getElementById("Winner_annoucement");
        while (winarea.firstChild) {
          winarea.removeChild(winarea.firstChild);
        }
        if (playeringame == "P") {
          const winmessage = document.createTextNode("AZUL VENCEU!!!");
          winarea.appendChild(winmessage);
          const wins = new Audio("./sounds/winsong.wav");
          wins.play();
          cont_azul++;
          setTimeout(mapReset, 1500);
        } else if (playeringame == "V") {
          const winmessage = document.createTextNode("VERMELHO VENCEU!!!");
          winarea.appendChild(winmessage);
          const wins = new Audio("./sounds/winsong.wav");
          wins.play();
          cont_vermelho++;
          setTimeout(mapReset, 1500);
        }
      }
    }
  }
  tabela_azul.textContent = cont_azul;
  tabela_vermelho.textContent = cont_vermelho;
}
function empate() {
  if (contador == 211) {
    const winarea = document.getElementById("Winner_annoucement");
    if (winarea.firstChild === null) {
      const winmessage = document.createTextNode("EMPATE");
      winarea.appendChild(winmessage);
      const empate = new Audio("./sounds/empate.mp3");
      empate.play();
      setTimeout(mapReset, 1500);
    }
}
}