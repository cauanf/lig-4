//Mapa e renderizar na tela as atualizações no array(map).
let map = [
  ["00", "01", "02", "03", "04", "05", "06"],
  ["10", "11", "12", "13", "14", "15", "16"],
  ["20", "21", "22", "23", "24", "25", "26"],
  ["30", "31", "32", "33", "34", "35", "36"],
  ["40", "41", "42", "43", "44", "45", "46"],
  ["50", "51", "52", "53", "54", "55", "56"]
];
let preto = "P";
let vermelho = "V";
const lig4Div = document.createElement("div");
lig4Div.id = "containerMap";
function mapRender(r) {
  r = turno();
  if (r === "P" || r === "V") {
    lig4Div.textContent = ""
  }
  for (let i = 0; i < map.length; i++) {
    for (let j = 0; j < map[i].length; j++) {
      if (map[i][j] !== "P" && map[i][j] !== "V") {
        let divMap = document.createElement("div");
        divMap.className = "coluna" + j;
        lig4Div.appendChild(divMap);
        document.body.appendChild(lig4Div);
      }
      if (map[i][j] === 'P') {
        let divMap = document.createElement("div");
        divMap.className = "preto";
        let discoP = document.createElement("div");
        discoP.className = "discoP"
        divMap.appendChild(discoP)
        lig4Div.appendChild(divMap);
        document.body.appendChild(lig4Div);
      }
      if (map[i][j] === 'V') {
        let divMap = document.createElement("div");
        divMap.className = "vermelho";
        let discoV = document.createElement("div");
        discoV.className = "discoV"
        divMap.appendChild(discoV)
        lig4Div.appendChild(divMap);
        document.body.appendChild(lig4Div);
      }
    }
  }
}

mapRender();
const mapReset = () => {
  map = [
    ["00", "01", "02", "03", "04", "05", "06"],
    ["10", "11", "12", "13", "14", "15", "16"],
    ["20", "21", "22", "23", "24", "25", "26"],
    ["30", "31", "32", "33", "34", "35", "36"],
    ["40", "41", "42", "43", "44", "45", "46"],
    ["50", "51", "52", "53", "54", "55", "56"]
  ];
  i0 = 5;
  i1 = 5;
  i2 = 5;
  i3 = 5;
  i4 = 5;
  i5 = 5;
  i6 = 5;
  contador = 0;
  const winarea = document.getElementById("Winner_annoucement");
  while (winarea.firstChild) {
    winarea.removeChild(winarea.firstChild);
  }
  mapRender();

}
