//Evento de clique e regras de jogada.

//TODO intercalar entre player.

//TODO adicionar "V" ou "P" na posição correta no map.

//TODO se a posição clicada já estiver ocupada acrescentar na de cima.
let contador = 0;
const turno = () => {
  let turn = "";
  if (contador % 2 === 0) {
    turn = preto;
  } else if (contador % 2 !== 0) {
    turn = vermelho;
  }
  contador++;

  return turn;
};
let i0 = 5;
let i1 = 5;
let i2 = 5;
let i3 = 5;
let i4 = 5;
let i5 = 5;
let i6 = 5;
const coluna = {
  coluna0: 0,
  coluna1: 1,
  coluna2: 2,
  coluna3: 3,
  coluna4: 4,
  coluna5: 5,
  coluna6: 6,
};
let botao = document.querySelectorAll("#containers > div");
function mostrar(event) {
  const winarea = document.getElementById("Winner_annoucement");
  if (
    (map[0][coluna[event.currentTarget.id]] != "P" &&
    map[0][coluna[event.currentTarget.id]] != "V")&&
    winarea.firstChild===null
  ) {
    let turn = turno();
    console.log(event.currentTarget.id);
    if (event.currentTarget === botao[0]) {
      if (map[i0][0] !== "P" && map[i0][0] !== "V") {
        map[i0][0] = turn;
        i0 -= 1;
        // console.log(map[i][0]);
      }
      if (map[0][0] === "P" && map[0][0] === "V") {
      }
    }
    if (event.currentTarget === botao[1]) {
      if (map[i1][1] !== "P" || map[i1][1] !== "V") {
        map[i1][1] = turn;
        i1 -= 1;
        // console.log(map[i][1]);
      }
    } else if (event.currentTarget === botao[2]) {
      if (map[i2][2] !== "P" || map[i2][2] !== "V") {
        map[i2][2] = turn;
        i2 -= 1;
        // console.log(map[i][2]);
      }
    } else if (event.currentTarget === botao[3]) {
      if (map[i3][3] !== "P" || map[i3][3] !== "V") {
        map[i3][3] = turn;
        i3 -= 1;
        // console.log(map[i][3]);
      }
    } else if (event.currentTarget === botao[4]) {
      if (map[i4][4] !== "P" || map[i4][4] !== "V") {
        map[i4][4] = turn;
        i4 -= 1;
        // console.log(map[i][4]);
      }
    } else if (event.currentTarget === botao[5]) {
      if (map[i5][5] !== "P" || map[i5][5] !== "V") {
        map[i5][5] = turn;
        i5 -= 1;
        // console.log(map[i][5]);
      }
    } else if (event.currentTarget === botao[6]) {
      if (map[i6][6] !== "P" || map[i6][6] !== "V") {
        map[i6][6] = turn;
        i6 -= 1;
        // console.log(map[i][6]);
      }
    }
    mapRender();
    vitoriaV(map, turno());
    vitoriaH(map, turno());
    vitoriaD(map, turno());
    empate();
    const move = new Audio("./sounds/move.wav");
    move.play();
  }
}

for (let i = 0; i < botao.length; i++) {
  // console.log(botao[i]);
  botao[i].addEventListener("click", mostrar, false);
}

let refresh = document.getElementById("btn")
refresh.addEventListener("click", () => {
  mapReset()
})